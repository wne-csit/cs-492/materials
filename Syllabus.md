<small>*Version 2020-Spring-1.0, Revised Sun Jan 12 23:27:51 UTC 2020*</small>



# Syllabus


## CS 492 COMPUTER SCIENCE CAPSTONE

This project-based course provides students the opportunity to demonstrate
their ability to synthesize and apply knowledge and skills acquired throughout
the computer science program. Using appropriate software engineering practices,
students will work in teams to substantially contribute to a significant, real
world, software project.

CREDITS 3 cr.<br>
PREREQUISITE CS 490


## Instructor Information

- Dr. Stoney Jackson
- EMAIL: Stoney.Jackson@wne.edu
- WEBSITE: https://wne.edu/~hjackson


## Office Hours

### Drop-in Office Hours

- Monday: 11-noon
- Tuesday: 11-noon
- Wednesday: 10-noon
- Thursday: 11-noon

All times are EDT.

To contact me during these times please text or call the number below.

- Phone/text: 413-782-1314

If we think sharing screens or video would be beneficial I can start a video
conference room after you text or call.


### Video Conference Appointments

Use this link to book a video conference appointment:

- https://stoneyjackson.youcanbook.me/

When you book an appointment, you will be sent a confirmation email that will
contain a link to the video conference room in which we'll meet. It won't be
active until a little before our appointment.



## It's in the Syllabus

<img src="http://www.phdcomics.com/comics/archive/phd051013s.gif"><br> <a
href="http://www.phdcomics.com/comics.php?f=1583">http://www.phdcomics.com/comics.php?f=1583</a>

Of course, not *everything* is in the syllabus, but many things are.

If you have a question, please check the syllabus first, and then ask if you
can't find the answer.


## Textbooks

There are no required, non-free textbooks for this course.

There is no textbook because the topics you that will be studied will vary wildly from student to student, course to course, and project to project.

Although much of what you are likely to study is freely available on line, you may want to allocating a personal budget of up to $150 for non-free materials that you think would help you along your journey.

## Required Materials

In addition to the textbook, to successfully complete this course you will need:

1.	**Laptop Computer:** You will need a laptop computer that you can bring to
    class sessions and can use at home. The brand and operating system
    (Windows, Mac OS X, Linux) is mostly unimportant (not Chromebook) – the
    software we will be using runs on all major operating systems and can be
    downloaded for free. It is expected that you will download and install
    required software as needed over the course of the semester.
2.	**Internet Access:** You will need Internet access for access to:
    1.	**GitLab** &mdash; We will use GitLab to host our code.
    2.	**Discord** &mdash; We will use Discord for team communication.
    3.	**Tutorials and articles** &mdash; I will suggest, and you will
        research on your own, tutorials and articles for you to learn new
        technologies and techniques we need.


## ACM Student Membership

It is *strongly* suggested that you have an <a
href="http://www.acm.org/membership/student/" target="_blank">ACM Student
Membership</a>. An ACM Student Membership costs only $19 and you get access to
a <a href="http://www.acm.org/membership/membership/student/benefits"
target="_blank">large list of benefits</a> including digital access to many of
O'Reilly's books.

The ACM is the world's largest educational and scientific computing
professional society. As a soon-to-be professional in computing, you should be
a member to keep up-to-date with advancements in the field. And it looks great
on your resume!


## Course Workload Expectations

***This is a three-credit project-based course. You should expect to spend, on
average, 9 hours per week on this class.***

You will spend 3 hours per week in class participating in Scrum meetings and
working with your team. In addition, you should expect to spend, on average, at
least 6 hours per week during the semester outside of class, writing code and
documentation, learning new tools and techniques, and communicating with the
team. (See *Definition of the Credit Hour* below.)

***The bulk of your work in this course will take place outside of the class
time. It is not possible to contribute the amount of work your team and the
class need to be successful entirely within the 3 hours of class time. If you
attempt to do so, you will not only receive a poor grade in the course, but
will also be letting down your classmates, and giving a poor impression of your
reliability and quality of work to your classmates and to members of the
professional community.***

***This is a chance to build your professional reputation - make sure you make
a good impression.***


## Definition of the Credit Hour

>Federal regulation defines a credit hour as an amount of work represented in
>intended learning outcomes and verified by evidence of student achievement
>that is an institutional established equivalence that reasonably approximates
>not less than –

>1.	One hour of classroom or direct faculty instruction and a minimum of two
>   hours of out of class student work each week for approximately fifteen
>   weeks for one semester or trimester hour of credit, or ten to twelve weeks
>   for one quarter hour of credit, or the equivalent amount of work over a
>   different amount of time; or
>2.	At least an equivalent amount of work as required in paragraph (1) of this
>   definition for other academic activities as established by the institution
>   including laboratory work, internships, practica, studio work, and other
>   academic work leading to the award of credit hours.

>---New England Association of Schools and Colleges, Commission on Institutions
>of Higher Education, [Policy on Credits and
>Degrees](http://cihe.neasc.org/downloads/POLICIES/Pp111_PolicyOnCreditsAndDegrees.pdf)


## Course Objectives

This course is intended to mimic, as closely as possible, the professional
software development team environment. Students will develop a significant
software system, employing knowledge gained from courses throughout the
program. They will work in groups to manage a project, following appropriate
project management techniques. They will develop the requirements, design,
implementation, and quality assurance plan, and analyze the project for
professional, social, and ethical issues.

Students will have to learn new technology and techniques on their own as
required for the specific project, showing the importance of continuing
professional development. They will have to reflect on their learning and make
frequent written and oral status reports on their individual and team progress.
They will be encouraged to contribute the results of their learning and their
work back to the professional community.

Students will explore and reflect on what it means to be a professional
software developer.


## Course-Level Student Learning Outcomes

Upon successful completion of this course, students will be able to:

* Develop a significant software system, employing knowledge gained from
  courses throughout the program.
* Work in groups to manage a project themselves, following all appropriate
  project management techniques.
* Develop requirements, design, implementation, and quality assurance.
* Follow a suitable process model.
* Analyze a project for professional, social, and ethical issues.
* Learn new models, techniques, and technologies as needed and appreciate the
  necessity of such continuing professional development.


## Course Topics

* Team Roles
  * Scrum master
  * Frontend developer
  * Backend developer
  * Documentation Writer
  * Code Merger
  * Code Reviewer
  * Quality Assurance and Tester
  * Database Designer/Implementer
* Software Development Process and Tools						
  * Using Gitlab issues, boards, epics, and milestones to plan, design,
    coordinate, implement, review, and test.					
* Infrastructure Skills and Tools
   * Containers
   * Continuous Integration
   * Testing frameworks
   * Documentation System
   * Versioning
   * Working agreements
   * Dependencies
   * Tech decisions
   * Build scripts
* Product	Skills
   *	Designing for Product Functionality
   *  Designing APIs
   *	Designing GUIs
   *	Writing Unit Tests
   *	Writing Functional Tests
   *	Writing Integration Tests
   *	Writing Code for Functionality
   *	Writing Documentation
* Professionalism


## Grading

- 18.75% Sprint 1<br>
- 28.75% Sprint 2<br>
- 28.75% Sprint 3<br>
- 23.75% Final Essay<br>
- 0% Team Presentation (Eliminated due to Covid-19)


## Sprints

Each sprint is made up of planning, working, reviewing, and retrospective
sessions. This document describes each of these sessions.

**Planning Sessions:** The first class of each multi-week sprint will be used for
the team to plan the upcoming sprint. This meeting will be facilitated by the
team's Scrum Master.  The Product Owner (the instructor) will be a participant
in this meeting, and will help with the planning.

**Working Sessions:** The classes in the middle of each multi-week sprint will
be used to participate in a Standup Meeting to keep your team informed of what
you have done, what you plan to do next, and any problems you are having. This
meeting will be facilitated by the team's Scrum Master. The Product Owner (the
instructor) may observe, depending on what other meetings are happening.

You will also use these classes to work in small groups on planning,
development, and learning. In addition, teams will be sending observers to
other teams' meetings to bring back information on what other teams are doing.
The Product Owner (the instructor) may be available for questions during these
classes, depending on what other meetings are happening.

**Review Sessions:** The next-to-last class of each multi-week sprint will be
used to review completed work. This meeting will be facilitated by the team's
Scrum Master.  The Product Owner (the instructor) will be a participant in this
meeting, will be the one you are presenting the completed work to, and will
make the determination of whether to accept work as "done".

**Retrospective Sessions:** The last class of each multi-week sprint will be
used to evaluate team performance, and decide on changes to make to team
process. This meeting will be facilitated by the team's Scrum Master. The
Product Owner (the instructor) will be a participant in this meeting, mostly as
an observer.

See [Sprint Evaluation](Sprint-Evaluation.md) for details on how each sprint
will be evaluated.


## Final Essay

At the end of this course, you will be asked to write a 1000 - 1500 word essay
reflecting on some aspect of your journey in this course. The exact topic will
be given later in this course.  The essay is due on the day of the Team
Presentation.

More details will be given about this requirement later in the class.


## Team Presentation

Final presentations by teams on their work and experience will be given during
our scheduled final exam period. Attire is business casual.

More details will be given about this requirement later in the class.


## Deliverables

Except for the final essay and presentation, all other deliverables must be
available on GitLab in an appropriate, designated location. Usually this will
be in the description or the comment of an issue, epic, or merge request, or in
a commit in a branch in an LFP project on GitLab. From the perspective of this
course, work not posted to GitLab does not exist and will not receive credit.

The final essay and presentation will be posted to Kidiak in a designated
assignment.

**Please do not submit assignments to me via email.** It is difficult for me to
keep track of them and I often fail to remember that they are in my mailbox
when it comes time to grade the assignment.


## Late Submissions

Late work will not be accepted.

Ack! Why?! Planning must happen in time for the planning meeting. The work you
do in a sprint is reviewed in specific meetings, and so must be on time.
Retrospective reports help you prepare for the retrospective meeting as well as
allow for the instructor to evaluate your accomplishments in the prior sprint.
The Final Essay and Team Presentation must be turned in on time so that they
can be graded in time to turn in a final grade.


## Getting Help

If you are struggling with the material or a project please see me as soon as
possible. Often a few minutes of individual attention is all that is needed to
get you back on track.

By all means, try to work out the material on your own, but ask for help when
you cannot do that in a reasonable amount of time. The longer you wait to ask
for help, the harder it will be to catch up.

**Asking for help or coming to see me during office hours is not bothering or
annoying me. I am here to help you understand the material and be successful in
the course.**


## Contacting Me

You may contact me by email (Stoney.Jackson@wne.edu), or see me in my office.
My office hours are listed at the beginning of this document, as well as a tool
to schedule an appointment with me if you cannot make my office hours.

**If you email me, please include “[CS-492]” in the subject line, so that I
know what course you are emailing about.**

**If you email me from an account other than your Western New England email,
please be sure that your name appears somewhere in the email, so that I know
who I am communicating with.**

<img src="http://www.phdcomics.com/comics/archive/phd042215s.gif"><br><a href="http://www.phdcomics.com/comics.php?f=1795">http://www.phdcomics.com/comics.php?f=1795</a>

You may expect that I will get back to you within 1 business day of your email
(that excludes weekends and when the school is closed), although you will
likely hear from me sooner.


## Code of Conduct/Classroom Civility

All students are expected to adhere to the policies as outlined in the
University’s Student Code of Conduct (see the [Student
Handbook](https://www1.wne.edu/student-affairs/Student%20Handbook.cfm)).


## Student Responsibilities

* Contribute to a class atmosphere conducive to learning for everyone by
  asking/answering questions, participating in class discussions. Don’t just
  lurk!
* Seek help when necessary
* Start assignments as soon as they are posted.  Do not wait until the due date
  to seek help/to do the assignments
* Expect to spend at least 9 hours of work per week on classwork.
* Each student is responsible for the contents of the textbook readings,
  handouts, and homework assignments.

## Extenuating Circumstances

If a circumstance beyond your control causes you to violate the above policies, you may request special considerations for your circumstance. Make a request as follows:

Complete the Extenuating Circumstance "quiz" on Kodiak. Submit hard copies of evidence to your instructor.

Appeals must be submitted as soon as possible once you are aware of the extenuating circumstances. Your instructor will determine the acceptability of an appeal, along with the specific accommodations that will be made.

*If you have any problems with the above instructions, please discuss your situation with your instructor.*

**NOTE: Do not use the extenuating circumstances quiz as a rapid 2-way communication. I process these regularly but often infrequently. If you need to interact with more more rapidly, please use email, office hours, or appointments.**


## Accessibility Statement

If you have a documented learning disability that requires special accommodation, please call the Learning Disability Services Office at 413-782-1257. I cannot accommodate you without express permission to do so from this office.

Note: One or more of your peers may be allowed to record audio from class session as part of an accommodation for personal use only. Therefore, be aware that your participation in class may be recorded.


## Academic Honesty

Any academic misconduct will be reported to the chair who will determine an appropriate penalty. Please see the Catalog and the Student Handbook for descriptions of integrity of scholarship and academic dishonesty.


## Acknowledgements

This course is heavily influenced by ongoing discussions between [Dr. Karl
Wurst](http://cs.worcester.edu/kwurst/) of Worcester State University starting
and [Stoney Jackson](http://www.wne.edu/~hjackson/) in the Fall 2014 semester (with 64 commuting hours, lunches, observing each
others courses, and more) and still continuing... It is also based on materials
from the [FOSS2Serve](http://foss2serve.org/index.php/Main_Page)/[Teaching Open
Source](http://teachingopensource.org/index.php/Main_Page)/[POSSE](http://teachingopensource.org/index.php/POSSE)
community.

## Change Clause

Changes may be made to the syllabus or these policies. Any changes made will be announced in class. Use the timestamp or version number on the document in GitLab or in Kodiak to determine if you have the most recent copy.


## Tentative Schedule

Date | Day | 1, 2 | 3, 4
:--: | :--: | :--: | :--:
1-14 | T | Launch | Launch
1-16 | R | Pre-Plan | Observe
1-21 | T | Work | Pre-Plan
1-23 | R | Plan | Work
1-28 | T | Work | Plan
1-30 | R | Work | Work
2-04 | T | Work | Work
2-06 | R | Work | Work
2-11 | T | Review | Work
2-13 | R | Retro | Work
2-18 | T | Plan | Work
2-20 | R | Work | Review
2-25 | T | Work | Retro
2-27 | R | Work | Plan
3-03 | T | Work | Work
3-05 | R | Work | Work
3-10 | T | Work (PO abs't) | Work (PO abs't)
3-12 | R | Work (PO abs't) | Work (PO abs't)
3-17 | T | Break  | Break
3-19 | R | Break  | Break
3-24 | T | Break (Covid-19)  | Break (Covid-19)
3-26 | R | Break (Covid-19)  | Break (Covid-19)
3-31 | T | Review | Work
4-02 | R | Plan   | Work
4-07 | T | Work (Ret-Rep)   | Review
4-09 | R | Work   | Plan
4-14 | T | Work   | Work (Ret-Rep)
4-16 | R | Work   | Work
4-21 | T | Work   | Work
4-23 | R | Work   | Work
4-28 | T | Review | Work
4-30 | R | Retro-Report | Review
5-05 | T | Demo | Retro-Report, Demo
5-06 | W | Essay | Essay

The table below shows the number of work sessions for each sprint for each team. Sprint 1 does not count the work session between pre-planning and planning. This table is provided to help aid planning.

(UPDATED for Covid-19)

Sprint | Work Sessions<br> Teams 1, 2 | Work Sessions <br> Teams 3, 4
:--: | :--: | :--:
1 | 4 | 6
2 | 7 | 5
3 | 6 | 5
Total | 17 | 16

## Copyright and License
#### &copy; 2020 Stoney Jackson and Karl Wurst
<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA2019
