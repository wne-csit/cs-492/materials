# Final Essay

## Overview

During this course you have faced several challenges that required you to identify, evaluate, and apply new information. Selecting the best source of information for a give problem is not easy in our field for a couple reasons. First, the technical nature of our field means it is often difficult to find just the right information to suit our specific problem. Second, because technology changes so rapidly, sources and the best practices they embody quickly become out of date.

## Charge

Write an essay describing one or more specific problems that you faced, and how you evaluated and identified appropriate information sources to solve your problem, and how you ultimately solved the problem by applying the information.

Your essay must be 1000-1500 words in length. Please use single-space with default margins and a traditional, standard font. Provide links to information sources and artifacts of your creation to support your essay. Follow ACM Citation Style and Reference Format [1].

[1] C. Rodkin. 2020. Citation Style and Reference Formats. Retrieved April 14, 2020 from https://www.acm.org/publications/authors/reference-formatting

## Due date

This is due the day after the Final Exam in this class.

## Submission

Submit a PDF to the appropriate Assignment module on Kodiak.

## Rough Rubric

* Mechanics: correct length and citation syntax
* English: correct grammar, syntax, organization, and flow
* Content: fully addresses the writing prompt
* Support: provides references to relevant sources and artifacts


---
&copy; 2020 Stoney Jackson

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/> This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA2019
